package problemabarriles;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BarrilesTest {

	@Test
	void testDisparoFallido() {
		int[][] matriz = {{0, 0, 1},{0, 0, 0},{0, 0, 0}};
		assertEquals("Meh", BarrilesSolucion.disparar(matriz, 1, 1, 0));
	}
	
	@Test
	void testDisparoFallido2() {
		int[][] matriz = {{1, 1, 1},{1, 0, 1},{1, 1, 1}};
		assertEquals("Meh", BarrilesSolucion.disparar(matriz, 1, 1, 0));
	}
	
	@Test
	void testSinBarriles() {
		int[][] matriz = {{0, 0, 0, 0},{0, 0, 0, 0},{0, 0, 0, 0}, {0, 0, 0, 0}};
		assertEquals("Meh", BarrilesSolucion.disparar(matriz, 1, 1, 0));
	}
	
	
	@Test
	void testExplosiones() {
		int[][] matriz = {{0, 0, 1, 0},{0, 0, 1, 0},{0, 1, 1, 0},{0, 0, 0, 1}};
		assertEquals("Explosion Nuclear", BarrilesSolucion.disparar(matriz, 1, 2, 0));		
	}
	
	@Test
	void testExplosiones2() {
		int[][] matriz = {{0, 1, 0},{0, 1, 0},{0, 0, 0}};
		assertEquals("Super Boom", BarrilesSolucion.disparar(matriz, 1, 1, 0));		
	}
	
	@Test
	void testExplosiones3() {
		int[][] matriz = {{0, 0, 1},{0, 0, 0},{0, 0, 0}};
		assertEquals("Boom", BarrilesSolucion.disparar(matriz, 0, 2, 0));		
	}
	
	@Test
	void testExplosiones4() {
		int[][] matriz = {{1, 1},{1, 1}};
		assertEquals("Explosion Nuclear", BarrilesSolucion.disparar(matriz, 0, 1, 0));		
	}
	
	@Test
	void testExplosiones5() {
		int[][] matriz = {{1, 0, 0, 1},{0, 0, 0, 0}, {0, 0, 0, 0}, {1, 0, 0, 1}};
		assertEquals("Boom", BarrilesSolucion.disparar(matriz, 0, 0, 0));		
	}
	
	@Test
	void testExplosiones6() {
		int[][] matriz = {{1, 0, 0},{0, 1, 1}, {0, 0, 0}};
		assertEquals("Super Ultra Boom", BarrilesSolucion.disparar(matriz, 0, 0, 0));		
	}
	
	@Test
	void testExplosiones7() {
		int[][] matriz = {{1, 0, 0, 0},{0, 1, 0, 0}, {0, 0, 1, 0},{0, 0, 0, 1}};
		assertEquals("Explosion Nuclear", BarrilesSolucion.disparar(matriz, 3, 3, 0));		
	}
	
	@Test
	void testBarrilesSinExplotar() {
		int[][] matriz = {{0, 0, 1, 0},{0, 0, 1, 0},{0, 1, 0, 0},{0, 0, 0, 1}};
		assertEquals("Super Ultra Boom", BarrilesSolucion.disparar(matriz, 1, 2, 0));
	}
}
