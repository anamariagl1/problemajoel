package problemabarriles;

import java.util.Scanner;

public class BarrilesSolucion {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int k = sc.nextInt();
		int[][] matriz = new int[k][k];

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matriz[i][j] = sc.nextInt();
			}
		}

		int f = sc.nextInt();
		int c = sc.nextInt();
		int contador=0;
		
		System.out.println(disparar(matriz, f, c, contador));

	}

	public static String disparar(int[][] matriz, int f, int c, int contador) {
		if (matriz[f][c] == 1) {
			contador=explosion(matriz, f, c, contador);
		}
		return comprobarContador(contador);
	}

	public static String comprobarContador(int contador) {		
		if (contador == 0) {
			return "Meh";
		} else if (contador == 1) {
			return "Boom";
		} else if (contador == 2) {
			return "Super Boom";
		} else if (contador == 3) {
			return "Super Ultra Boom";
		} else
			return "Explosion Nuclear";
	}

	public static int explosion(int[][] matriz, int f, int c, int contador) {
		contador++;
		matriz[f][c] = 0;
		if (!meSalgo(f + 1, c, matriz) && matriz[f + 1][c] == 1)
			contador=explosion(matriz, f + 1, c, contador);
		if (!meSalgo(f, c + 1, matriz) && matriz[f][c + 1] == 1)
			contador=explosion(matriz, f, c + 1, contador);
		if (!meSalgo(f + 1, c + 1, matriz) && matriz[f + 1][c + 1] == 1)
			contador=explosion(matriz, f + 1, c + 1, contador);
		if (!meSalgo(f - 1, c, matriz) && matriz[f - 1][c] == 1)
			contador=explosion(matriz, f - 1, c, contador);
		if (!meSalgo(f, c - 1, matriz) && matriz[f][c - 1] == 1)
			contador=explosion(matriz, f, c - 1, contador);
		if (!meSalgo(f - 1, c - 1, matriz) && matriz[f - 1][c - 1] == 1)
			contador=explosion(matriz, f - 1, c - 1, contador);
		if (!meSalgo(f + 1, c - 1, matriz) && matriz[f + 1][c - 1] == 1)
			contador=explosion(matriz, f + 1, c - 1, contador);
		if (!meSalgo(f - 1, c + 1, matriz) && matriz[f - 1][c + 1] == 1)
			contador=explosion(matriz, f - 1, c + 1, contador);
		return contador;
	}

	public static boolean meSalgo(int f, int c, int[][] matriz) {
		if (f < 0 || c < 0 || f >= matriz.length || c >= matriz[0].length) {
			return true;
		} else {
			return false;
		}
	}
}
